#include <iostream>
#include <vector>
#include <cassert>

namespace dyn_array_lib {

template<typename T>
DynArray<T>::DynArray(size_t size):
    size_(size),
    capacity_(size == 0 ? 1 : size),
    elts_(new T[capacity_])
{}

template<typename T>
DynArray<T>::DynArray(const DynArray<T>& rhs):
    size_(rhs.size_),
    capacity_(rhs.capacity_),
    elts_(new T[capacity_])
{
    std::cout << "copy" << std::endl;
    for(size_t i = 0; i < size_; ++i) {
        elts_[i] = rhs.elts_[i];
    }
}

// DynArray<T>& operator=(const DynArray<T>& rhs);
// bool operator==(const DynArray& other) const;

template <typename T>
DynArray<T>::~DynArray(){
    delete[] elts_;
}

template <typename T>
size_t DynArray<T>::push_back(const T& elt) {
    if(size_ == capacity_) {
        grow();
    }
    elts_[size_] = elt;
    size_ += 1;
    return size() - 1;
}

template <typename T>
const T& DynArray<T>::operator[](size_t i) const {
    // DynArray: out of range
    assert(i >= 0 && i < size_);
    return *(elts_ + i);
}

template <typename T>
T& DynArray<T>::operator[](size_t i) {
    return const_cast<T&>(
        static_cast<const DynArray<T>& >(*this)[i] );
}

template <typename T>
std::ostream& operator<<(std::ostream& stream,
    const DynArray<T>& rhs) {
    std::cout << "(";
    for(size_t i = 0; i < rhs.size() - 1; ++i) {
        std::cout << rhs[i] << " ";
    }
    std::cout << rhs[rhs.size() - 1] << ")";
}

template <typename T>
void DynArray<T>::grow() {
    capacity_ *= 2;
    T* new_elts = new T[capacity_];
    for(size_t i = 0; i < size_; ++i) {
        new_elts[i] = elts_[i];
    }
    delete[] elts_;
    elts_ = new_elts;
}

template <typename T>
void DynArray<T>::swap(DynArray<T>& rhs) {
    size_ = size_ ^ rhs.size_;
    rhs.size_ = size_ ^ rhs.size_;
    size_ = size_ ^ rhs.size_;

    capacity_ = capacity_ ^ rhs.capacity_;
    rhs.capacity_ = capacity_ ^ rhs.capacity_;
    capacity_ = capacity_ ^ rhs.capacity_;

    T* tmp_elts = elts_;
    elts_ = rhs.elts_;
    rhs.elts_ = tmp_elts;
}

template <typename T>
void swap(DynArray<T>& a, DynArray<T>& b) {
    a.swap(b);
}

} //namespace dyn_array_lib
