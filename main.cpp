#include <iostream>
#include <cstdlib>

#include "building.h"
#include "dyn_array.h"

using namespace std;
using namespace building;
using namespace dyn_array_lib;

int main(int argc, char ** argv)
{
    cout << "Enter the building number (-99 to end)" << endl;
    DynArray<Building*> street(0);

    int nb = -rand() % 100;
    while (nb != -99){
        street.push_back(new Building(nb));
        nb = -rand() % 100;
    };

    cout << street << endl;

    DynArray<Building*> tmp = street;
    using std::swap;
    swap<Building*>(street, tmp);


    for(size_t i = 0; i < street.size(); ++i) {
        delete street[i];
    }

    cout << tmp << endl;

    return 0;
}
