#ifndef BUILDING_H
#define BUILDING_H

#include <iosfwd>

namespace building {

class Building {
public:
    Building();
    Building(int id);
    Building(const Building& rhs);
    ~Building();
    Building& operator=(const Building& rhs);
    friend std::ostream& operator<<(std::ostream& stream, const Building& rhs);

private:
    int id_;
};

} //namespace building

#endif //BUILDING_H
