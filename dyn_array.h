#ifndef DYN_ARRAY_H
#define DYN_ARRAY_H

#include <iosfwd>

namespace dyn_array_lib{

template <typename T>
class DynArray
{
public:
    explicit DynArray(size_t size);
    DynArray(const DynArray<T>& rhs);
    DynArray<T>& operator=(const DynArray<T>& rhs);
    bool operator==(const DynArray& other) const;
    ~DynArray();
    /// Return the index of the new element
    size_t push_back(const T& elt);
    size_t size() const {return size_;}
    const T& operator[](size_t i) const;
    T& operator[](size_t i);

    void swap(DynArray<T>& rhs);
private:
    size_t size_;
    size_t capacity_;
    T* elts_;
    void grow();
};

template <typename T>
std::ostream& operator<<(std::ostream& stream, const DynArray<T>& rhs);

template <typename T>
void swap(DynArray<T>& a, DynArray<T>& b);

} // namespace dyn_array_lib


#include "dyn_array.hpp"

#endif // DYN_ARRAY_H
