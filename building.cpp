#include <iostream>
#include <limits>

#include "building.h"
#include "dyn_array.h"

using namespace building;
using namespace std;

Building::Building(): id_(numeric_limits<int>::min())
{
    cout << "default constructor at " << this << endl;
}

Building::Building(int id): id_(id)
{
    cout << "constructor " << id_ << " at " << this << endl;
}

Building::Building(const Building& rhs)
{
    cout << "copy constructor " << id_ << " at " << this << " from " << &rhs
        << endl;
}

Building::~Building()
{
    cout << "destructor at " << this << endl;
}

Building& Building::operator=(const Building& rhs)
{
    cout << this << "(" << id_ << ")::" << "\033[31m" << "operator="
    << "\033[0m" << "(" << &rhs << "(" << rhs.id_ << "))" << endl;
    if(this == &rhs) {return *this;}
    id_ = rhs.id_;
    return *this;
}

namespace building {
ostream& operator<<(ostream& stream, const Building& rhs)
{
    return stream << "id = " << rhs.id_ << " at " << &rhs;
}
} // namespace building
